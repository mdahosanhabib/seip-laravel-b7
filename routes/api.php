<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/cart', function () {

    $cartItems = [
        [
            'id' => 1,
            'title' => 'I Phone',
            'unitPrice' => 100000,
            'qty' => 1,
            'itemTotalPrice' => 100000
        ],
        [
            'id' => 2,
            'title' => 'My Phone',
            'unitPrice' => 150000,
            'qty' => 1,
            'itemTotalPrice' => 150000
        ]
    ];

    return response()->json([
        'status' => 'ok',
        'data' => $cartItems
    ]);
});
