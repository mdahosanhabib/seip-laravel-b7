<?php

namespace App\View\Composers;

use App\Models\Cart;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
 
class FrontendComposer
{
 
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = Category::all();

        $cartItemsCount = Auth::user()->cartItems->count();

        $view->with([
            'categories' => $categories,
            'cartItemCount' => $cartItemsCount
        ]);
    }
}