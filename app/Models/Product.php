<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    // protected $fillable = ['title', 'description', 'price'];
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
    public function comments()
    {
        // return $this->hasMany(Comment::class)->orderBy('id','desc');
        return $this->morphMany(Comment::class, 'commentable')->orderBy('id','desc');
    }

}
