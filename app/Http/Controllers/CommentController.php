<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Notifications\NewComment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
class CommentController extends Controller
{
    public function store( Request $request, Product $product){
        $product->comments()->create([
            'body'=> $request->body,
            'Commented_by' => Auth::id()
        ]);

        $user=User::where('email','cseahosan@gmail.com')->first();
        Notification::send($user, new NewComment($product));
        return redirect()->back();

    }
}
