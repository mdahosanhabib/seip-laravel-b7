<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use Image;
use Barryvdh\DomPDF\Facade\Pdf;


class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->paginate(15);
        return view('products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $colors = Color::pluck('title', 'id')->toArray();

        return view('products.create', compact('categories', 'colors'));
    }

    public function show(Product $product)
    {
        // $products = Product::find($id);
        return view('products.show', compact('product'));
    }

    public function store(ProductRequest $request)
    {
        $requestData = [
            'category_id' => $request->category_id,
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'is_active' => $request->is_active ? true : false,
            'image' => $this->uploadImage($request->file('image'))
        ];

        $product = Product::create($requestData);

        $product->colors()->attach($request->colors);

        return redirect()
            ->route('products.index')
            ->withMessage('Successfully Created');
    }

    public function edit(Product $product)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $colors = Color::pluck('title', 'id')->toArray();
        $selectedColors = $product->colors()->pluck('id')->toArray();
        return view('products.edit', compact('product', 'categories', 'colors', 'selectedColors'));
    }

    public function update(ProductRequest $request, Product $product)
    {
        // $products = Product::find($id);

        $requestData = [
            'title' => $request->title,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'price' => $request->price,
            'is_active' => $request->is_active ? true : false,
        ];

        if ($request->hasFile('image')) {
            $requestData['image'] = $this->uploadImage($request->file('image'));
        }

        $product->update($requestData);
        $product->colors()->sync($request->colors);

        return redirect()
            ->route('products.index')
            ->withMessage('Successfully Updated');
    }

    public function downloadPdf()
    {
        $products = Product::all();
        $pdf = Pdf::loadView('products.pdf', compact('products'));
        return $pdf->download('product-list.pdf');
    }

    public function destroy(Product $product)
    {
        // $products = Product::find($id);
        $product->delete();
        
        // Session::flash('message', 'Successfully deleted');
        // return redirect()
        //         ->route('products.index')
        //         ->with('message', 'Successfully deleted');

        return redirect()
            ->route('products.index')
            ->withMessage('Successfully deleted');
    }

    public function trash()
    {
        $products = Product::onlyTrashed()->get();
        return view('products.trash', compact('products'));
    }

    public function restore($id)
    {
        $products = Product::onlyTrashed()->find($id);
        $products->restore();

        return redirect()
            ->route('products.trash')
            ->withMessage('Successfully restored');
    }

    public function delete($id)
    {
        $product = Product::onlyTrashed()->find($id);
        $product->forceDelete();
        $product->colors()->detach();

        return redirect()
            ->route('products.trash')
            ->withMessage('Successfully deleted');
    }

    public function uploadImage($image)
    {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        // $image->move(storage_path('/app/public/products'), $fileName); 

        Image::make($image)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/products/' . $fileName);

        return $fileName;
    }
}
