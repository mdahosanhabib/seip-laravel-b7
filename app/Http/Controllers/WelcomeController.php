<?php

namespace App\Http\Controllers;

<<<<<<< HEAD
use App\Models\Cart;
=======
use App\Models\Carousel;
>>>>>>> eb6717c1325c81e17c1530cae6a93f7b7bda074a
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    public function welcome()
    {
        if ($keyword = request('keyword')) {
            $products = Product::latest()
                        ->where('title', 'LIKE', "%{$keyword}%")
                        ->paginate(15);
        } else {
            $products = Product::latest()->paginate(15);
        }
        $carousels = Carousel::latest()->get();
        // dd($carousels);
        return view('welcome', compact('products', 'carousels'));
    }

    public function productList(Category $category)
    {
        return view('products', compact('category'));
    }

    public function productDetails(Product $product)
    {
        return view('product', compact('product'));
    }

    public function shoppingCart()
    {
        $cartItems = Auth::user()->cartItems;

        return view('cart', compact('cartItems'));
    }
}
