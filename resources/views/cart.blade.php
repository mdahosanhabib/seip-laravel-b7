<x-frontend.master>

    <div class="container marketing">
        <br><br><br><br>
        Shopping cart

        <table class="table table-info">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Item</th>
                    <th>Unit Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="cartItems">
                @foreach($cartItems as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->product->title }}</td>
                    <td>{{ $item->product->price }}</td>
                    <td><input type="number" value="{{ $item->qty }}" class="qty-input" /></td>
                    <td>{{ $item->product->price * $item->qty }}</td>
                    <td><button class="remove-btn">Remove</button></td>
                </tr>
                @endforeach

            </tbody>
        </table>

        Total Price: <span id="totalPrice">0</span>


    </div><!-- /.container -->
    
    @push('js')
        <script src="{{ asset('ui/frontend/js/cart.js') }}"></script>
    @endpush

</x-frontend.master>