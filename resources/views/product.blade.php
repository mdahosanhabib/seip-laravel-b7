<x-frontend.master>


    <div class="container marketing">
        <br><br><br><br>
        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="col-lg-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <img height="350" src="{{ asset('storage/products/'.$product->image) }}" alt="{{ $product->title }}" />
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->

            <div class="col-lg-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ Str::limit($product->title, 40) }}</h3>
                    </div>
                    <div class="card-body">
                        <p>{{ Str::limit($product->description, 50) }}</p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-primary btn-sm" href="#">Add to Card</a>
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
        <section>
          @auth
                <form action="{{ route('products.comments.store', $product->id) }}" method="POST">
                    @csrf
                    <x-forms.textarea name="body" label="Your Comment" /></textarea>
                    <button class="btn btn-primary"  type="submit">Submit</button>
                </form>

            @else
            <a class="btn btn-info" href="{{url('/login')}}">Login to Comment</a>
            @endauth
          

           
        </section>
        <section style="background-color: rgb(250, 249, 249); padding-top:20px;">
            <h3 >
                Previous Comments
            </h3>
            <ul>
                @foreach ($product->comments as $comment)
                    
               
                <li style="color: rgb(57, 117, 117)">
                   <div>
                    <h5>
                       {{$comment->commentedBy->name}} 
                        <small style="font-size: 12px">
                          commented {{ $comment->created_at->diffForHumans()}}
                         ({{date('d-M-Y')}})
                        </small>
                    </h5>
                    <p>{{$comment->body}}</p>
                   </div>
                </li>

                @endforeach
            </ul>

        </section>
       

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
</x-frontend.master>